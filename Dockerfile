ARG IMAGE_NAME
FROM ${IMAGE_NAME}:10.2-devel-ubuntu18.04 as build

ARG DEBIAN_FRONTEND=noninteractive
ENV APPS="libuv1-dev libssl-dev libhwloc-dev git build-essential cmake"

# Set timezone and create user
RUN export DEBIAN_FRONTEND=noninteractive; \
    apt-get update; \
    ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime; \
    apt-get install -y tzdata; \
    dpkg-reconfigure --frontend noninteractive tzdata; \
    apt-get clean all; \
    # Create user account
    groupadd -g 98 docker; \
    useradd --uid 99 --gid 98 docker; \
    echo 'docker:docker' | chpasswd; \
    usermod -aG sudo docker;

# Install default apps
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y $APPS && apt-get clean all && echo "Set disable_coredump false" >> /etc/sudo.conf

# Prepare xmrig
WORKDIR /home/docker

RUN git clone -b dev https://github.com/xmrig/xmrig /home/docker/xmrig && \
        sed -i "s/kDefaultDonateLevel =.*/kDefaultDonateLevel = 0;/g; s/kMinimumDonateLevel =.*/kMinimumDonateLevel = 0;/g" /home/docker/xmrig/src/donate.h && \
        mkdir xmrig/build && cd xmrig/build && cmake .. && make -j$(nproc) && cd /home/docker

RUN git clone https://github.com/xmrig/xmrig-cuda /home/docker/xmrig-cuda && \
        mkdir xmrig-cuda/build && cd xmrig-cuda/build && \
        cmake .. -DCUDA_LIB=/usr/local/cuda/lib64/stubs/libcuda.so -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda && \
        make -j$(nproc) && cp *.so /home/docker/xmrig/build/

FROM ${IMAGE_NAME}:10.2-devel-ubuntu18.04

ENV COIN="monero"
ENV POOL_URL="pool.supportxmr.com:5555"
ENV WALLET="458om9UQwSVcNhSoYmfncPhqwiCQLR7LoATAoKu4Xn1DFfaBEzRkZZrYbWmYfgz2VJQws3MTTLBTXcqAxn8urNki5VZDQKw"
ENV WORKER="Docker"

# Set timezone and create user
RUN apt-get update; \
    ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime; \
    apt-get install -y sudo tzdata; \
    dpkg-reconfigure --frontend noninteractive tzdata; \
    apt-get clean all; \
    # Create user account
    groupadd -g 98 docker; \
    useradd --uid 99 --gid 98 docker; \
    echo 'docker:docker' | chpasswd; \
    usermod -aG sudo docker; \
    # Prevent error messages when running sudo
    echo "Set disable_coredump false" >> /etc/sudo.conf;

WORKDIR /home/docker

COPY "mine.sh" "/home/docker/mine.sh"
RUN chmod +x /home/docker/mine.sh && mkdir -p /home/docker/xmrig/
COPY --from=build /home/docker/xmrig/build/* /home/docker/xmrig/

USER docker

CMD ["./mine.sh"]
