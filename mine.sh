#!/bin/sh

echo "Project:  xmrig ${MINERV}"
echo "Author:   lnxd"
echo "Base:     Ubuntu 20.04"
echo "Target:   Unraid"
echo "Donation: ${FEE} ${DONATE}%"
echo ""
echo "Running xmrig with the following flags:"
echo "--url=${POOL_URL} --coin=${COIN} --user=${WALLET}.${WORKER} --randomx-wrmsr=-1 --randomx-no-rdmsr ${OPTIONS}"

./xmrig/xmrig --url=${POOL_URL} --coin=${COIN} --user=${WALLET}.${WORKER} --cpu-max-threads-hint=${MAX_CPU} --randomx-wrmsr=-1 --randomx-no-rdmsr ${OPTIONS}
